import json
from math import sqrt, ceil

import matplotlib.pyplot as plt


import numpy as np
import bottleneck as bn

CALIB_AFTER = 500
WHICH_MEASUREMENTS = [2,3]
MAX_DIST = 3000
def calib_error():

    with open('result_50k_2_3.5.json') as json_file:
        data = json.load(json_file)


    measurements = list(np.transpose([[measurement['m_result'] for measurement in dut['measurements']] for dut in data['component']]))


    measurements = measurements[WHICH_MEASUREMENTS[0]:WHICH_MEASUREMENTS[1]]

    fig, ax = plt.subplots()

    # ax.violinplot(list(mes_t_by_measurement), positions=pos, points=20, widths=0.3, vert=True, showmeans=True, showextrema=True, showmedians=True)
    for y in measurements:
        y = y[:MAX_DIST]
        # ym = y
        ym = bn.move_mean(y, window=20, min_count=5)
        # ym = np.fft.fft(ym)

        ax.plot(ym)
        for ca in range(CALIB_AFTER, len(y)+1, CALIB_AFTER):
            start = ca - CALIB_AFTER
            end = ca
            plt.axvline(x=ca, c='red')
            m, b = np.polyfit(range(start, end), y[start:end], 1)
            plt.plot(range(start,end), m*range(start,end) + b, 'yo')

    for ca in range(CALIB_AFTER, len(y), CALIB_AFTER):
        plt.axvline(x=ca, c='red')
    # for ax in axes.flat:
    #     ax.set_yticklabels([])
    plt.xlabel('#measurement')
    plt.ylabel('Measurement value [0,1] with lowpass filter')
    plt.title('Linear drift over time')
    plt.show()


calib_error()