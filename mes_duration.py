import json
from math import sqrt, ceil

import matplotlib.pyplot as plt


import numpy as np


def mes_duration_violin():

    with open('result.json') as json_file:
        data = json.load(json_file)


    # mes_t_by_measurement = np.transpose([[measurement['m_time'] for measurement in dut['measurements']] for dut in data['component']])
    mes_t_by_measurement = np.transpose([[measurement['m_time'] for measurement in dut['measurements']] for dut in data['component']])

    pos = list(range(mes_t_by_measurement.shape[0]))
    fs = 10


    # add random factor
    # mes_t_by_measurement = mes_t_by_measurement + [np.random.normal(0, std, size=mes_t_by_measurement.shape[1]) for std in np.random.rayleigh(1, size=mes_t_by_measurement.shape[0])*0.1 ]

    fig, ax = plt.subplots()

    # ax.violinplot(list(mes_t_by_measurement), positions=pos, points=20, widths=0.3, vert=True, showmeans=True, showextrema=True, showmedians=True)
    ax.boxplot(list(mes_t_by_measurement), 0, '')
    # for ax in axes.flat:
    #     ax.set_yticklabels([])
    plt.xlabel('Measurement ID')
    plt.ylabel('Duration (s)')
    plt.title('Res of measurements')
    plt.show()


mes_duration_violin()