import json
from math import sqrt, ceil

import matplotlib.pyplot as plt


import numpy as np


def error_histogram():

    with open('result_calvar.json') as json_file:
        data = json.load(json_file)

    calibration_intervals = [c['cal_int'] for c in data['components']]
    errors_accum = [np.sum([(1 if dut['dut_result'] else 0) for dut in c['c']]) for c in data['components']]

    plt.plot(calibration_intervals, errors_accum)
    plt.xlabel('Calibraiton interval')
    plt.ylabel('Number of errors')
    plt.title('Errors dependend on calibration interval')
    plt.show()


error_histogram()