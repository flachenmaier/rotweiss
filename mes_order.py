import json
from math import sqrt, ceil

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import animation, cm


def sort_measurement(m):
    index, duration, errors = m
    return duration - 0.1*errors

def drawchart(until_dut):
    errors_accum = np.sum(
        [[measurement['m_is_error'] for measurement in dut['measurements']] for dut in data['component']][0:until_dut], axis=0)


    points = [(i, t_by_measurement[i], errors_accum[i]) for i in range(len(t_by_measurement))]
    points.sort(key=sort_measurement)
    # norm = plt.Normalize(errors_accum.min(), errors_accum.max())
    colors = plt.cm.autumn_r(norm([i[2] for i in points]))
    # bar = ax.bar(x=range(len(points)), height=[i[1] for i in points], color=colors)
    for i, b in enumerate(bar):
        b.set_height(points[i][1])
        b.set_color(colors[i])
    # bar.set_height([i[1] for i in points])
    plt.xticks(range(len(points)), [i[0] for i in points])
    # cd.ylabel('Anzahl der Fehelr')
    plt.xlabel('Measurement ID')
    plt.ylabel('Duration (s)')
    plt.title(str(until_dut) + ' devices tested')


with open('result.json') as json_file:
    data = json.load(json_file)

# mes_t_by_measurement = np.transpose([[measurement['m_time'] for measurement in dut['measurements']] for dut in data['component']])
t_by_measurement = [mes['m_time'] for mes in data['component'][0]['measurements']]

fig, ax = plt.subplots()
bar = ax.bar(x=range(len(t_by_measurement)), height=np.max(t_by_measurement))
norm = plt.Normalize(0, 50)
cd = plt.colorbar(cm.ScalarMappable(norm=norm, cmap='autumn_r'), ticks=[norm.vmin, norm.vmax])

drawchart(50000)
plt.show()
exit(0)

Writer = animation.writers['ffmpeg']
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

# animator = animation.FuncAnimation(fig, drawchart, frames=range(100, 300, 100))
animator = animation.FuncAnimation(fig, drawchart, frames=range(100, 50000, 100))
animator.save('IMPORVED_RAND_50k_dui_seed2_3.5.mp4', writer=writer)