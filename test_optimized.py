import time
from DUT import DUT
import matplotlib.pyplot as plt
import numpy as np
import json
import sys

def main():
    # yield 3 ==> ~90%
    # yield 4.4 ==> ~99.99%
    #my_dut = DUT(None,True,3.8)
    # my_dut = DUT()
    my_dut = DUT(2,True,3.5)

    meastime, nmeas, nport, meas, ports, expyield = my_dut.info()
    print("DUT: meas. time= ", meastime, " | measurements= ", nmeas, " | ports= ", nport, " | expected yield = ", expyield)

    error_count = 0
    X = []
    Y = []
    t=0
    TOTAL_TIME_NO_ABORT = 0
    TOTAL_TIME_FIXED_ORDER = 0
    TIME_CALIB = 0
    TOTAL_TIME_OPTIM_ORDER = 0
    MTIME_WORKING = 0
    MTIME_BROKEN_AFTER = 0
    MTIME_BROKEN_BEFORE = 0

    data= {}
    data['component']=[]


    for x in range(50000):
        my_dut.new_dut()
        dut={}
        dut['dut_id'] = x
        dut['measurements']=[]

        if x % 500 == 0:
            TIME_CALIB += my_dut.calibrate()


        failed_mes = False

        meas_time_working = 0
        meas_time_broken_before = 0
        meas_time_broken_after = 0
        for i in range(0, nmeas):
            t, result, dist = my_dut.gen_meas_idx(i)
            measurement = {}
            measurement['m_id'] = i
            measurement['m_time'] = meas[i].meas_time
            measurement['m_result'] = dist
            measurement['m_is_error'] = 1 if dist >= 1 else 0
            meas_time_working += meas[i].meas_time
            if dist >= 1:
                failed_mes = True
            if not failed_mes:
                meas_time_broken_before += meas[i].meas_time
                TOTAL_TIME_FIXED_ORDER += meas[i].meas_time
            else:
                meas_time_broken_after += meas[i].meas_time

            dut['measurements'].append(measurement)


        t, res, dist = my_dut.get_result()
        dut['dut_result'] = res

        if res:
            MTIME_WORKING += meas_time_working
        else:
            MTIME_BROKEN_AFTER += meas_time_broken_after
            MTIME_BROKEN_BEFORE += meas_time_broken_before

        data['component'].append(dut)

        TOTAL_TIME_NO_ABORT = t
        X.append(t)
        Y.append(dist)
        if not res:
            error_count += 1

    error_dut, error_meas = my_dut.get_errordutcount()
    print("Total: ",t, "s ", x+1, " ( ", error_count, " | ", error_dut, " | ", error_meas, " ) ==> ", (x+1-error_count)/(x+1) )


    print('TOTAL_TIME_NO_ABORT', TOTAL_TIME_NO_ABORT-TIME_CALIB, 'TOTAL_TIME_FIXED_ORDER', TOTAL_TIME_FIXED_ORDER-TIME_CALIB, 'TIME_CALIB', TIME_CALIB)

    print('total testing time (h)', t//3600)
    print('total time testing working devices', MTIME_WORKING//3600)
    print('total time testing broken devices (before error occured)', MTIME_BROKEN_BEFORE//3600)
    print('total time testing broken devices (after error occured)', MTIME_BROKEN_AFTER//3600)
    pass
    # write json log
    # outfile = open('result.json', 'w')
    # json.dump(data, outfile, indent=2)

    # plot results
    # timeAxis = [x / 3600. for x in X]
    # plt.xlabel('time [h]')
    # plt.plot(timeAxis,Y)
    # plt.axhline(y=1., xmin=0, xmax=1, color='r')
    # plt.show()



if __name__ == "__main__":
    # execute only if run as a script
    main()