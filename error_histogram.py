import json
from math import sqrt, ceil

import matplotlib.pyplot as plt


import numpy as np


def error_histogram():

    with open('result.json') as json_file:
        data = json.load(json_file)

    errors_accum = np.sum([[measurement['m_is_error'] for measurement in dut['measurements']] for dut in data['component']], axis=0)

    nmes = len(errors_accum)
    x_dim = ceil(sqrt(nmes))
    while not nmes/x_dim % 1 == 0:
        x_dim += 1
    y_dim = nmes//x_dim

    fig, ax = plt.subplots()
    field = np.reshape(errors_accum, [y_dim, x_dim])


    # Limits for the extent
    x_start = 3.0
    x_end = 9.0
    y_start = 6.0
    y_end = 12.0

    extent = [x_start, x_end, y_start, y_end]

    im = ax.imshow(field, extent=extent, cmap='autumn_r', origin='lower')

    plt.tick_params(
        axis='both',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        left=False,  # ticks along the top edge are off
        right=False,  # ticks along the top edge are off
        labelbottom=False,
        labelleft=False
    )

    jump_x = (x_end - x_start) / (2.0 * x_dim)
    jump_y = (y_end - y_start) / (2.0 * y_dim)
    x_positions = np.linspace(start=x_start, stop=x_end, num=x_dim, endpoint=False)
    y_positions = np.linspace(start=y_start, stop=y_end, num=y_dim, endpoint=False)

    for y_index, y in enumerate(y_positions):
        for x_index, x in enumerate(x_positions):
            label = field[y_index, x_index]
            text_x = x + jump_x
            text_y = y + jump_y
            ax.text(text_x, text_y, label, color='black', ha='center', va='center')

    ax.set_title("How often does earch error occur?\nEach square represents on measurment")
    fig.tight_layout()
    plt.show()


error_histogram()